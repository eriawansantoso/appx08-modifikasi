package santoso.eriawan.appx08

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppCompatActivity(), View.OnClickListener{

    val arrayWarna = arrayOf("BLUE","YELLOW","GREEN","BLACK")
    lateinit var adapterSpin : ArrayAdapter<String>
    var bg : String = ""
    var fontheader : String = ""
    var fSubTitle : Int = 0
    var fDetail : Int = 0
    var judul : String = ""
    var detail : String = ""

    val sb1 = object : SeekBar.OnSeekBarChangeListener{
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            fSubTitle = progress.toFloat().toInt()
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {

        }

    }
    val sb2 = object : SeekBar.OnSeekBarChangeListener{
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            fDetail = progress.toFloat().toInt()
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {

        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        adapterSpin = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayWarna)
        spWarnaBg.adapter = adapterSpin
        var paket: Bundle? = intent.extras
        edDetail.setText(paket?.getString("detail"))
        edTitle.setText(paket?.getString("judul"))
        sbSubtitle.setOnSeekBarChangeListener(sb1)
        sbDetail.setOnSeekBarChangeListener(sb2)
        spWarnaBg.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(baseContext, "Tidak ada yang dipilih", Toast.LENGTH_SHORT).show()
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                Toast.makeText(baseContext, adapterSpin.getItem(position), Toast.LENGTH_SHORT)
                    .show()
                bg = adapterSpin.getItem(spWarnaBg.selectedItemPosition).toString()
            }
        }
        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rbBlue -> fontheader = "Blue"
                R.id.rbYellow -> fontheader = "Yellow"
                R.id.rbGreen -> fontheader = "Green"
                R.id.rbBlack -> fontheader = "Black"
            }
            btSimpan.setOnClickListener(this)
        }
    }


    override fun onClick(v: View?) {
        judul = edTitle.text.toString()
        detail = edDetail.text.toString()
        Toast.makeText(this, "Berhasil Menyimpan", Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun finish() {
        var intent = Intent()
        intent.putExtra("bgColor",bg)
        intent.putExtra("fhColor",fontheader)
        intent.putExtra("tHead",fSubTitle)
        intent.putExtra("tTitle",fDetail)
        intent.putExtra("title",judul)
        intent.putExtra("isidetail",detail)
        setResult(Activity.RESULT_OK,intent)
        super.finish()
    }


}